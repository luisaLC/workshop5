<?php

class Database{
    
    function getConnection() {
      $connection = new mysqli('localhost:3306', 'root', 'lulitrix', 'php_web2');
      if ($connection->connect_errno) {
        printf("Connect failed: %s\n", $connection->connect_error);
        die;
      }
      return $connection;
    }
}