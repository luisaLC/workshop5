<?php
require ("Database.php");
class Student{
    function getAll($limit=10){
        $connectionObject = new Database();
        
        $conn = $connectionObject->getConnection();
        
        $sql = "select * from students LIMIT $limit";
        $resultObj =  $conn->query($sql);
        $results = [];

        if($resultObj->num_rows > 0){
            while($row = $resultObj-> fetch_array()){
                array_push($results, $row);
            }
        }
        $conn->close();
        return $results;
    }
}